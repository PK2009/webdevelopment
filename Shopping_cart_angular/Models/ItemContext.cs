﻿using System;
using Microsoft.EntityFrameworkCore;

namespace class_angular_shoppingcart
{
	public class ItemContext : DbContext
    {
		public ItemContext(DbContextOptions<ItemContext> options) : base(options) { }
        public ItemContext() { }
        public DbSet<ItemDetails> ItemDetails { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace class_angular_shoppingcart
{
    public class ItemDetails
    {
        public ItemDetails()
        {
        }

		public ItemDetails(int id, string name, int price, string image, string description, string addedby)
        {
            Item_ID = id;
            Item_Name = name;
            Item_Price = price;
            Image_Name = image;
            Description = description;
            AddedBy = addedby;
        }
    		[Key] 
        public int Item_ID { get; set; }
        [Required]
        [Display(Name = "Item_Name")]
        public string Item_Name { get; set; }
        [Required]
        [Display(Name = "Item_Price")]
        public int Item_Price { get; set; }
        [Required]
        [Display(Name = "Image_Name")]
        public string Image_Name { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "AddedBy")]
        public string AddedBy { get; set; }
    }
}

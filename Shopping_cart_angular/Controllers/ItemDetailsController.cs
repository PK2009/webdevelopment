﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace class_angular_shoppingcart.Controllers
{
	[Produces("application/json")]
    [Route("api/ItemDetailsAPI")]
    public class ItemDetailsController : Controller
    {
        // GET: /<controller>/
		private readonly ItemContext _context;

		public ItemDetailsController(ItemContext context)
		{
			_context = context;
        }


        [HttpGet]
        [Route("Details")]

		public IEnumerable<ItemDetails> GetItemDetails()
        {
            IEnumerable<ItemDetails> items = _context.ItemDetails;
            //List<ItemDetails> items = new List<ItemDetails>();
            //items.Add(new ItemDetails(1, "dvd", 10, "CD.png", "GoT", "dino"));
            return items;
        }
        public IActionResult Index()
        {
            return View();
        }

		// GET api/values/5
        [HttpGet]
        [Route("Details/{ItemName}")]
        public IEnumerable<ItemDetails> GetItemDetails(string ItemName)
        {
            //return _context.ItemDetails.Where(i => i.Item_ID == id).ToList(); ;
            IEnumerable<ItemDetails> items = _context.ItemDetails.Where(i => i.Item_Name.Contains(ItemName)).ToList();
            return items;
        }
        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}

    }
}

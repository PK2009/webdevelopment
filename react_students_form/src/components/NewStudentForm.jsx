import React, {Component, PropTypes} from 'react';
import TextField from 'material-ui/lib/text-field'; import FlatButton from 'material-ui/lib/flat-button';
import Toggle from 'material-ui/lib/toggle';
import styles from '../styles/rootStyles';

class NewStudentForm extends Component { 
    static propTypes = {
        onCreateNewStudent: PropTypes.func.isRequired
        }
initialState = { firstName: '', lastName: '', email: '', isGood: false
};
constructor(props) {
super(props);
this.handleFirstNameChange = this.handleFirstNameChange.bind(this); this.handleLastNameChange = this.handleLastNameChange.bind(this); this.handleEmailChange = this.handleEmailChange.bind(this); this.handleGoodChange = this.handleGoodChange.bind(this); this.handleSaveClick = this.handleSaveClick.bind(this);
}
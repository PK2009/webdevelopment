import React, {Component} from 'react';
import Header from './Header';
import NewStudentForm from './NewStudentForm'; import StudentList from './StudentList';
import Footer from './Footer';
import styles from '../styles/rootStyles';

class MainDashboard extends Component {
    constructor(props) {
        super(props);
        this.handleCreateNewStudent = this.handleCreateNewStudent.bind(this); 
    }
handleCreateNewStudent(student) {
    let oldStudents = this.state && this.state.students ? this.state.students : []; 
    let newStudents = [student, ...oldStudents];
this.setState({
students: newStudents
}); }
    
    render() {
let students = this.state && this.state.students ? this.state.students : [];
return (
<div style={styles.container}>
<Header />
<div style={styles.mainContentArea}>
<NewStudentForm onCreateNewStudent={this.handleCreateNewStudent} />
<StudentList students={students}/>
</div>
<Footer />
    </div>
); }
}
export default MainDashboard;
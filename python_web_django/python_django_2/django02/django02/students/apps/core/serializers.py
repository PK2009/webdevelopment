from rest_framework import serializers
from .models import University, Student
class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)
        fields = self.context['request'].query_params.get('fields') 
        if fields:
            fields = fields.split(',')
# Drop any fields that are not specified in the `fields` argument. 
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

class UniversitySerializer(DynamicFieldsModelSerializer, serializers.ModelSerializer): 
    class Meta:
        model = University 
        fields = ('id', 'name')
class StudentSerializer(DynamicFieldsModelSerializer, serializers.ModelSerializer): 
    class Meta:
        model = Student
        fields = ('id', 'first_name', 'last_name', 'university_id')
from django.conf.urls import url
from django.views.generic import TemplateView
from rest_framework import routers
from core.views import StudentViewSet, UniversityViewSet

router = routers.DefaultRouter() 
router.register(r'students', StudentViewSet) 
router.register(r'universities', UniversityViewSet)
app_name = 'students'
urlpatterns = router.urls

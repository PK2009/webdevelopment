﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using crazy.Models;

namespace crazy.Controllers
{
    
    public class ValuesController : Controller
    {
		Player_Manager player_Manager = new Player_Manager();     

		[HttpGet]
		[Route("api/[controller]")]
		public IEnumerable<Players> Get()
		{
			return player_Manager.GetAll.ToList();
		}


        [HttpPost] 
		[Route("api/[controller]/[action]")]
		[ActionName("Post01")]
		public StatusCodeResult Post01([FromBody] Players s)
        {
            if (s == null)
            {
                return new StatusCodeResult(500); //internal error 
			}
            
			if (player_Manager.GetAll.Any(_ => _.Player_id == s.Player_id))
            {
				if (player_Manager.EditPlayer(s))
                {
                    return new StatusCodeResult(200); //success 
				}
else
{
                        return new StatusCodeResult(404); //not found
                    }
                }


                    else
                    {
				player_Manager.AddPlayers(s); //dbContext.Companies.Add(company);
                                          //await dbContext.SaveChangesAsync();
				return new StatusCodeResult(201); //created
                    }
       }


    }
}

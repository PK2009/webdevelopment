import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
import { CounterComponent } from './components/counter/counter.component';
import { testComponent } from './components/test/test.component';
import { gamemenuComponent } from './components/gamemenu/gamemenu.component';
import { test2Component } from './components/test2/test2.component';
import { test3Component } from './components/test3/test3.component';



@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        testComponent,
        gamemenuComponent,
        test2Component,
        test3Component,
        HomeComponent,
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'gamemenu', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'test', component: testComponent },
            { path: 'test2', component: test2Component },
            { path: 'test3', component: test3Component },
            { path: 'gamemenu', component: gamemenuComponent },
            { path: 'fetch-data', component: FetchDataComponent },
            { path: '**', redirectTo: 'gamemenu' }
        ])
    ]
})
export class AppModuleShared {
}

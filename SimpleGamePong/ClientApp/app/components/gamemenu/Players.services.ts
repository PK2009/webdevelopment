﻿import { Component } from '@angular/core'; 
import { Http, Response } from '@angular/http'; 
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import { Players } from './Players.data';

@Injectable()
export class PlayerServiceProvider {
    private getUrl: string = "http://localhost:5000/api/values";
    private postUrl: string = "http://localhost:5000/api/values/Post01";

    constructor(private http: Http) {}
    getPlayers(): Observable<Players[]> {
        var resps: Observable<Response>;
        resps = this.http.get(this.getUrl);
        var studs: Observable<Players[]>
        studs = resps.map<Response, Players[]>(resp=>resp.json());
        return studs
    }

    addplayer(play: Players): Observable<Response> {
        var res: Observable<Response>;
        res= this.http.post(this.postUrl, play).map(resm => resm.json());
        return res;
    }
}
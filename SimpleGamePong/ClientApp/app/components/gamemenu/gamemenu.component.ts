﻿import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Response } from '@angular/http';
import { Players } from './Players.data';
import { PlayerServiceProvider } from './Players.services';
import { Router } from '@angular/router';
import { elementAt } from 'rxjs/operator/elementAt';

@Component({
    selector: 'gamemenu',
    templateUrl: './gamemenu.component.html',
    providers: [ PlayerServiceProvider ]
    })

    export class gamemenuComponent implements OnInit
{
    Players: Players[];
    play: Players;
    j:any; 
    
         constructor(private router: Router, private PlayerService: PlayerServiceProvider){
        this.play = new Players();
             this.Players = [];
            
    }
      ngOnInit()
      {
          this.PlayerService.getPlayers().subscribe(plays => this.Players = plays);
      }


    SavePlayer()

    {
          if (this.play.Player_id in this.Players)
            {
                    this.router.navigate(['/test']);

            }
        else
        {
             this.PlayerService.addplayer(this.play).subscribe((msg: Response) => alert(msg.statusText));
             this.router.navigate(['/test']);
        }
            
       }  
}

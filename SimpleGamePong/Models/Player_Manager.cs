﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace crazy.Models
{
	public class Player_Manager
	{
		List<Players> _characs;
		public Player_Manager()
		{
			try
			{
				if (File.Exists("chars.txt"))
				{
					_characs = ReadPlayerList().ToList();
				}
				else
				{
					_characs = new List<Players>()
					{
						new Players("dino",1, 0)
					};
					WritetoPlayerList(_characs);
				}
			}
			catch (IOException ioe)
			{
			}
		}


		void WritetoPlayerList(IEnumerable<Players> characters)
		{
			var output = JsonConvert.SerializeObject(characters);
			File.WriteAllText("chars.txt", output);

		}

		IEnumerable<Players> ReadPlayerList()
		{
			return JsonConvert.DeserializeObject<List<Players>>(File.ReadAllText("chars.txt"));
		}

		public IEnumerable<Players> GetAll { get { return _characs; } }
		public IEnumerable<Players> GetPlayersBycharacters(int id)
		{
			return _characs.Where(o => o.Player_id.Equals(id)).ToList();
		}

		public void AddPlayers(Players p)
		{
			_characs.Add(p);
			var output = JsonConvert.SerializeObject(_characs);
			File.WriteAllText("chars.txt", output);
		}

        
		public bool EditPlayer(Players s)
		{
			var _s = _characs.FirstOrDefault(_ => _.Player_id == s.Player_id); if (_s == null) return false;
			_s.Player_id = s.Player_id;
			_s.Player_name = s.Player_name;
			_s.Player_state = s.Player_state;
            return true;
	}

	}
}

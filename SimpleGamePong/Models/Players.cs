﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace crazy.Models
{
    public class Players
    {
		public Players(string player_name, int player_id, int player_state)
        {
			Player_name = player_name;
			Player_id = player_id;
			Player_state = player_state;
        }


		[HiddenInput]
        public int Player_id { get; set; }

		[HiddenInput]
		public int Player_state { get; set; }

		[Required]
        [Display(Name = "Player's Name")]
        public string Player_name { get; set; }

		    }
}

package main.java.com.movie.web.controller;

import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RequestMethod; 
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class MovieController {
    @RequestMapping(value = "/getJson", method=RequestMethod.GET)
    @ResponseBody
    public HashMap<String, String> getAJson() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Game of", "Thrones");
        return map;
    }
}
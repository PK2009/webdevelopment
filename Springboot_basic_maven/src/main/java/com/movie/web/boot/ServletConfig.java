package main.java.com.movie.web.boot;

import javax.servlet.Filter;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer; import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import main.java.com.movie.web.filter.NocacheFilter;

@Configuration
public class ServletConfig {
    @Bean
public EmbeddedServletContainerCustomizer portCustomizer() {
    return (container -> { container.setPort(8090); }); 
}

@Bean
public FilterRegistrationBean noCacheFilter() {
    Filter filter = new NocacheFilter();
    FilterRegistrationBean registration = new FilterRegistrationBean(); 
    registration.setFilter(filter);
    registration.setOrder(1);
    return registration;
}
}
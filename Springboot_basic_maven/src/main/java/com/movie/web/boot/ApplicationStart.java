package main.java.com.movie.web.boot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer; 
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.movie.web.boot", "com.movie.web.controller"})

public class ApplicationStart extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApplicationStart.class); 
}

    public static void main (String[] args) { 
        SpringApplication.run(ApplicationStart.class, args);
    }
}


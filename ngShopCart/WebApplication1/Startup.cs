using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;
using Microsoft.Extensions.Logging;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ItemContext>(options =>
                              options.UseInMemoryDatabase("name"));
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigins",
                    builder =>
                    {
                        builder.AllowAnyOrigin();
                    });
            });

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCors("AllowAllOrigins");

            app.UseStaticFiles();

            var context = app.ApplicationServices.GetService<ItemContext>();
            AddStoreData(context);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }

        private static void AddStoreData(ItemContext context)
        {
            var i = new ItemDetails(1, "Atlanta", 9, "https://assets.nflxext.com/us/boxshots/ghd/80123779.jpg", "Two cousins, with different views on art versus commerce...", "dino");
            context.ItemDetails.Add(i);

            i = new ItemDetails(2, "Billions", 8, "https://assets.nflxext.com/us/boxshots/166/80067080.jpg", "U.S. Attorney Chuck Rhoades goes after hedge fund king...", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(3, "Black Mirror", 10, "https://assets.nflxext.com/us/boxshots/ghd/70075028.jpg", "A television anthology series that shows the dark side...", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(4, "Breaking Bad", 8, "https://assets.nflxext.com/us/boxshots/166/70105286.jpg", "A high school chemistry teacher diagnosed with inoperable...", "alex"); context.ItemDetails.Add(i);
            i = new ItemDetails(5, "Homeland", 9, "https://assets.nflxext.com/us/boxshots/166/70180305.jpg", "A bipolar CIA operative becomes convinced a prisoner of war...", "alei = x"); context.ItemDetails.Add(i);
            i = new ItemDetails(6, "Game of Thrones", 11, "https://assets.nflxext.com/us/boxshots/166/70152478.jpg", "Nine noble families fight for control over the mythical...", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(7, "House of Cards", 11, "https://assets.nflxext.com/us/boxshots/166/70177674.jpg", "A Congressman works with his equally conniving wife to exact revenge...", "alex"); context.ItemDetails.Add(i);
            i = new ItemDetails(8, "Master of None", 10, "https://assets.nflxext.com/us/boxshots/166/80000948.jpg", "The personal and professional life of Dev, a 30-year-old actor in New York.", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(9, "Narcos", 9, "https://assets.nflxext.com/us/boxshots/166/80025272.jpg", "A chronicled look at the criminal exploits of Colombian drug lord Pablo Escobar.", "alex"); context.ItemDetails.Add(i);
            i = new ItemDetails(10, "Orange is the new Black", 8, "https://assets.nflxext.com/us/boxshots/166/70242309.jpg", "The story of Piper Chapman, a woman in her thirties who is sentenced to fifteen months in prison...", "alex"); context.ItemDetails.Add(i);
            i = new ItemDetails(11, "Silicon Valley", 11, "https://assets.nflxext.com/us/boxshots/166/70301999.jpg", "Follows the struggle of Richard Hendricks, a silicon valley engineer trying to...", "alex"); context.ItemDetails.Add(i);
            i = new ItemDetails(12, "Stranger Things", 9, "https://assets.nflxext.com/us/boxshots/166/80077209.jpg", "When a young boy disappears, his mother, a police chief, and his friends must confront terrifying forces...", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(13, "The Americans", 9, "https://assets.nflxext.com/us/boxshots/166/70269395.jpg", "Two Soviet intelligence agents pose as a married couple to spy on the American government.", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(14, "Veep", 8, "https://assets.nflxext.com/us/boxshots/166/70229242.jpg", "Former Senator Selina Meyer finds that being Vice President of the United States is nothing like...", "dino"); context.ItemDetails.Add(i);
            i = new ItemDetails(15, "Westworld", 11, "https://assets.nflxext.com/us/boxshots/166/80066892.jpg", "Set at the intersection of the near future and the reimagined past, explore a world in which every human appetite...", "alex"); context.ItemDetails.Add(i);
            context.SaveChanges();
        }
    }
}

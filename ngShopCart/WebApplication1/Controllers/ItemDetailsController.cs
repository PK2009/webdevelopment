﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api/ItemDetails")]
    public class ItemDetailsController : Controller
    {
        private readonly ItemContext _context;
        private ItemDetails _itemdetails;
        //private ItemDetailsList _items;

        public ItemDetailsController(ItemContext context)
        {
            _context = context;
        }

        // GET: api/ItemDetails
        [HttpGet]
        [Route("Details")]
        public IEnumerable<ItemDetails> GetItemDetails()
        {
            IEnumerable<ItemDetails> shop = _context.ItemDetails;
            return shop;
        }
        //[HttpGet]
        //public IEnumerable<ItemDetails> GetItemDetails()
        ////public IEnumerable<ItemDetails> GetItemDetails()
        //{
        //    _items = new ItemDetailsList(_context);
        //    return _context.ItemDetailsList;
        //}

        // GET: api/ItemDetails/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetItemDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var itemDetails = await _context.ItemDetails.SingleOrDefaultAsync(m => m.Item_ID == id);

            if (itemDetails == null)
            {
                return NotFound();
            }

            return Ok(itemDetails);
        }

        // GET api/values/5
        [HttpGet]
        [Route("Details/{ItemName}")]
        public IEnumerable<ItemDetails> GetItemDetails(string ItemName)
        {
            //return _context.ItemDetails.Where(i => i.Item_ID == id).ToList(); ;

            IEnumerable<ItemDetails> items = _context.ItemDetails.Where(i => i.Item_Name.Contains(ItemName)).ToList();
            return items;
        }

        // PUT: api/ItemDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutItemDetails([FromRoute] int id, [FromBody] ItemDetails itemDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != itemDetails.Item_ID)
            {
                return BadRequest();
            }

            _context.Entry(itemDetails).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ItemDetails
        [HttpPost]
        public async Task<IActionResult> PostItemDetails([FromBody] ItemDetails itemDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ItemDetails.Add(itemDetails);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetItemDetails", new { id = itemDetails.Item_ID }, itemDetails);
        }

        // DELETE: api/ItemDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItemDetails([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var itemDetails = await _context.ItemDetails.SingleOrDefaultAsync(m => m.Item_ID == id);
            if (itemDetails == null)
            {
                return NotFound();
            }

            _context.ItemDetails.Remove(itemDetails);
            await _context.SaveChangesAsync();

            return Ok(itemDetails);
        }

        private bool ItemDetailsExists(int id)
        {
            return _context.ItemDetails.Any(e => e.Item_ID == id);
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ItemContext : DbContext
    {
        //public ItemContext(DbContextOptions<ItemContext> options)
        //    : base(options) { }
        public ItemContext(DbContextOptions options)
            : base(options)
        {
            var debugging = 0;
        }
        //public ItemContext()
        //    : base() { }
        //public DbSet<List<ItemDetails>> ItemDetailsList { get; set; }
        public DbSet<ItemDetails> ItemDetails { get; set; }
    }

}

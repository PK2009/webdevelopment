﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    // **** No, you don;t need this because the Context already defines a DbSet: public DbSet<ItemDetails> ItemDetails { get; set; }
    //
    //public class ItemDetailsList
    //{
    //    public ItemDetailsList()
    //    {

    //    }

    //    public ItemDetailsList(ItemContext db)
    //    {
    //        //Products = GetProducts();
    //        //ItemContext//var context = app.ApplicationServices.GetService<ItemContext>();
    //        AddStoreData(db);
    //    }

    //    private static void AddStoreData(ItemContext context)
    //    {
    //        var l = new List<ItemDetails>();

    //        var i = new ItemDetails(1, "Atlanta", 9, "https://assets.nflxext.com/us/boxshots/ghd/80123779.jpg", "Two cousins, with different views on art versus commerce...", "dino");
    //        l.Add(i);

    //        i = new ItemDetails(2, "Billions", 8, "https://assets.nflxext.com/us/boxshots/166/80067080.jpg", "U.S. Attorney Chuck Rhoades goes after hedge fund king...", "dino"); l.Add(i);
    //        i = new ItemDetails(3, "Black Mirror", 10, "https://assets.nflxext.com/us/boxshots/ghd/70075028.jpg", "A television anthology series that shows the dark side...", "dino"); l.Add(i);
    //        i = new ItemDetails(4, "Breaking Bad", 8, "https://assets.nflxext.com/us/boxshots/166/70105286.jpg", "A high school chemistry teacher diagnosed with inoperable...", "alex"); l.Add(i);
    //        i = new ItemDetails(5, "Homeland", 9, "https://assets.nflxext.com/us/boxshots/166/70180305.jpg", "A bipolar CIA operative becomes convinced a prisoner of war...", "alei = x"); l.Add(i);
    //        i = new ItemDetails(6, "Game of Thrones", 11, "https://assets.nflxext.com/us/boxshots/166/70152478.jpg", "Nine noble families fight for control over the mythical...", "dino"); l.Add(i);
    //        i = new ItemDetails(7, "House of Cards", 11, "https://assets.nflxext.com/us/boxshots/166/70177674.jpg", "A Congressman works with his equally conniving wife to exact revenge...", "alex"); l.Add(i);
    //        i = new ItemDetails(8, "Master of None", 10, "https://assets.nflxext.com/us/boxshots/166/80000948.jpg", "The personal and professional life of Dev, a 30-year-old actor in New York.", "dino"); l.Add(i);
    //        i = new ItemDetails(9, "Narcos", 9, "https://assets.nflxext.com/us/boxshots/166/80025272.jpg", "A chronicled look at the criminal exploits of Colombian drug lord Pablo Escobar.", "alex"); l.Add(i);
    //        i = new ItemDetails(10, "Orange is the new Black", 8, "https://assets.nflxext.com/us/boxshots/166/70242309.jpg", "The story of Piper Chapman, a woman in her thirties who is sentenced to fifteen months in prison...", "alex"); l.Add(i);
    //        i = new ItemDetails(11, "Silicon Valley", 11, "https://assets.nflxext.com/us/boxshots/166/70301999.jpg", "Follows the struggle of Richard Hendricks, a silicon valley engineer trying to...", "alex"); l.Add(i);
    //        i = new ItemDetails(12, "Stranger Things", 9, "https://assets.nflxext.com/us/boxshots/166/80077209.jpg", "When a young boy disappears, his mother, a police chief, and his friends must confront terrifying forces...", "dino"); l.Add(i);
    //        i = new ItemDetails(13, "The Americans", 9, "https://assets.nflxext.com/us/boxshots/166/70269395.jpg", "Two Soviet intelligence agents pose as a married couple to spy on the American government.", "dino"); l.Add(i);
    //        i = new ItemDetails(14, "Veep", 8, "https://assets.nflxext.com/us/boxshots/166/70229242.jpg", "Former Senator Selina Meyer finds that being Vice President of the United States is nothing like...", "dino"); lAdd(i);
    //        i = new ItemDetails(15, "Westworld", 11, "https://assets.nflxext.com/us/boxshots/166/80066892.jpg", "Set at the intersection of the near future and the reimagined past, explore a world in which every human appetite...", "alex"); l.Add(i);

    //        context.ItemDetailsList.Add(l);
    //        context.SaveChanges();
    //    }
    //}

    public class ItemDetails
    {
        public ItemDetails()
        {

        }

        public ItemDetails(ItemContext db)
        {
            //Products = GetProducts();
            //ItemContext//var context = app.ApplicationServices.GetService<ItemContext>();
            //AddStoreData(db);
        }

        //public IList<Product> Products { get; private set; }
        //private IList<Product> GetProducts()
        //{
        public ItemDetails(int id, string name, int price, string image, string description, string addedby)
        {
            Item_ID = id;
            Item_Name = name;
            Item_Price = price;
            Image_Name = image;
            Description = description;
            AddedBy = addedby;
        }

        [Key]
        public int Item_ID { get; set; }

        [Required]
        [Display(Name = "Item_Name")]
        public string Item_Name { get; set; }

        [Required]
        [Display(Name = "Item_Price")]
        public int Item_Price { get; set; }

        [Required]
        [Display(Name = "Image_Name")]
        public string Image_Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "AddedBy")]
        public string AddedBy { get; set; }
    }
}
